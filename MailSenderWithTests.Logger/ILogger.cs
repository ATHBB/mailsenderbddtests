﻿using System;

namespace MailSenderWithTests.Logger
{
    public interface ILogger
    {
        void AddLogEntry(string message);
        void AddLogError(string message, Exception ex);
        void InitializeLogger(string filePath);
        void Close();
    }
}