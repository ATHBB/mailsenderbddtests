﻿using MailSenderWithTests.Data.Application;
using Serilog;
using System;

namespace MailSenderWithTests.Logger
{
    public class Logger : ILogger
    {
        public Logger(string filePath)
        {
            InitializeLogger(filePath);
        }

        public void InitializeLogger(string filePath)
        {
            Log.Logger = new LoggerConfiguration()
                    .WriteTo.LiterateConsole()
                    .WriteTo.RollingFile(filePath,shared:true)
                    .CreateLogger();
        }

        public void AddLogEntry(string message)
        {
            if (string.IsNullOrWhiteSpace(message)) throw new Exception(ExceptionTexts.emptyMessageToLog);
            Log.Information(message);
        }

        public void Close()
        {
            Log.CloseAndFlush();
        }
        public void AddLogError(string message, Exception ex)
        {
            if (string.IsNullOrWhiteSpace(message)) throw new Exception(ExceptionTexts.emptyMessageToLog);
            if (ex == null) throw new Exception(ExceptionTexts.exceptionToLogIsNull);
            Log.Error(ex, message);
        }
    }
}
