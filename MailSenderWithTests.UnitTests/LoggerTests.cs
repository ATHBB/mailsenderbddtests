﻿using MailSenderWithTests.Data.Application;
using System;
using Xunit;

namespace MailSenderWithTests.UnitTests
{
    public class LoggerTests
    {
        public static object[] Data()
        {
            return new object[]
                {
                    new object[] { null, null,ExceptionTexts.emptyMessageToLog},
                    new object[] { "aaa", null , ExceptionTexts.exceptionToLogIsNull }
                };
        }

        [Fact]
        public void TestAddLogEntryExceptions()
        {
            var logger = new Logger.Logger();
            var ex = Assert.Throws<Exception>(() => logger.AddLogEntry(null));
            Assert.Contains(ExceptionTexts.emptyMessageToLog, ex.Message);
        }

        [Theory]
        [MemberData(nameof(Data))]
        public void AddLogErrorEmptyMessageTest(string message, Exception ex,string exceptionText)
        {
            var logger = new Logger.Logger();
            var expectedEx = Assert.Throws<Exception>(() => logger.AddLogError(message, ex));
            Assert.Contains(exceptionText, expectedEx.Message);
        }

    }
}
