﻿Feature: ApplicationIterationTests

@mytag
Scenario: IncrementIterationTest
	Given I Have initiated iteration with value 2
	When Iteration is incremented
	And I get iteration from settings
	Then Value should be 3

Scenario: GetIterationTest
	Given I Have initiated iteration with value 2
	When I get iteration from settings
	Then Value should be 2
