﻿using MailSenderWithTests.DataParser.ApplicationIteration;
using System;
using System.Configuration;
using TechTalk.SpecFlow;
using Xunit;

namespace MailSenderWithTests.BDDTests.ApplicationIterationTests
{
    [Binding]
    public class ApplicationIterationTestsSteps
    {
        IterationValue currentIteration;
        int? Iteration;

        [Given(@"I Have initiated iteration with value (.*)")]
        public void GivenIHaveInitiatedIterationWithValue(int iterationValue)
        {
            InitiateIteration(iterationValue);
        }
        [When(@"Iteration is incremented")]
        public void WhenIterationIsIncremented()
        {
            currentIteration.IncrementIterationValue();
        }

        [When(@"I get iteration from settings")]
        public void WhenIGetIterationFromSettings()
        {
            Iteration = currentIteration.GetIterationValue().Value;
        }
        [Then(@"Value should be (.*)")]
        public void ThenValueShouldBe(int expectedValue)
        {
            Assert.Equal(expectedValue, Iteration.Value);
        }
        private void InitiateIteration(int Iteration)
        {
            var configuration = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[nameof(Iteration)].Value = Iteration.ToString();
            configuration.Save();
            ConfigurationManager.RefreshSection("appSettings");
            currentIteration = new IterationValue();
        }
    }
}
