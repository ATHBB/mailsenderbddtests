﻿using MailSenderWithTests.Data.Types;
using TechTalk.SpecFlow;
using Xunit;

namespace MailSenderWithTests.BDDTests.MailToSendTests
{
    [Binding]
    public class MailToSendTestsSteps
    {
        MailToSend testObject = new MailToSend(new Person());
        [Given(@"I have sent mail")]
        public void GivenIHaveSentMail()
        {
            
        }
        
        [When(@"I successfully sent mail")]
        public void WhenISuccessfullySentMail()
        {
            testObject.AfterSuccessfullySend();
        }
        
        [When(@"I unsuccessfully sent mail")]
        public void WhenIUnsuccessfullySentMail()
        {
            testObject.AfterErrorSend();
        }

        [When(@"I sent mail after receiving one error")]
        public void WhenISentMailAfterReceivingOneError()
        {
            testObject.AfterErrorSend();
        }

        [Then(@"The mail status should be true")]
        public void ThenTheMailStatusShouldBeTrue()
        {
            var expected = true;
            Assert.Equal(expected, testObject.WasSend);
        }
        
        [Then(@"The mail status should be false")]
        public void ThenTheMailStatusShouldBeFalse()
        {
            var expected = false;
            Assert.Equal(expected, testObject.WasSend);
        }
        [Then(@"The mail status should be (.*)")]
        public void ThenTheMailStatusShouldBe(bool p0)
        {
            
        }


        [Then(@"The number of unseccessfull send messages should be (.*)")]
        public void ThenTheNumberOfUnseccessfullSendMessagesShouldBe(int p0)
        {
            var expected = 1;
            Assert.Equal(expected, testObject.ErrorCounter);
        }

    }
}
