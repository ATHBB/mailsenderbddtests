﻿Feature: MailToSendTests
	 

@mytag
Scenario: AfterSuccessfullySendTest
	Given I have sent mail
	When I successfully sent mail
	Then The mail status should be True

Scenario: AfterErrorSendCheckWasSendValueTest
	Given I have sent mail
	When I unsuccessfully sent mail
	Then The mail status should be False

Scenario: AfterErrorSendCheckErrorCounterValueTest
	Given I have sent mail
	When I sent mail after receiving one error
	Then The number of unseccessfull send messages should be 1
