﻿Feature: LoggerTests
To test logger functions
I test them

@AddLogEntry
Scenario: AddLogEntry
Given I have messageLog as MessageToLog  
When I try to add entry to log
Then The message should appear in log file

@AddLogError
Scenario: AddLogError
Given I have message as MessageToLog and exceptionMessage as invalidEmailValue
When I try to log error 
Then The messageLog and exceptionText should appear in log file