﻿using System;
using System.IO;
using TechTalk.SpecFlow;
using Xunit;

namespace MailSenderWithTests.BDDTests.LoggerTests
{
    [Binding]
    public class LoggerTestsSteps
    {
        Logger.Logger _logger;
        const string testDirectoryPath = @"TestData\Log";
        const string testFileName = "testLog";
        string Message = "";
        Exception GivenException;

        public LoggerTestsSteps()
        {
            PrepareDirectoryForTestData();
            _logger = new Logger.Logger(GetTestFilePathWithoutDate());
        }

        void PrepareDirectoryForTestData()
        {
            if (Directory.Exists(testDirectoryPath))
            {
                if (File.Exists(GetTestFilePath())) File.Delete(GetTestFilePath());
            }
            else
                Directory.CreateDirectory(testDirectoryPath);
        }


        [Given(@"I have messageLog as (.*)")]
        public void GivenIHaveMessageLogAsMessageToLog(string messageLog)
        {
            Message = messageLog;
        }

        [When(@"I try to add entry to log")]
        public void WhenITryToAddEntryToLog()
        {
            _logger.AddLogEntry(Message);
            _logger.Close();
        }

        [Then(@"The message should appear in log file")]
        public void ThenTheMessageShouldAppearInLogFile()
        {
            using (FileStream fileStream = new FileStream(GetTestFilePath(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (StreamReader fileReader = new StreamReader(fileStream))
            {
                Assert.Contains(Message, fileReader.ReadLine());
            }
        }

        [Given(@"I have message as (.*) and exceptionMessage as (.*)")]
        public void GivenIHaveMessageAsMessageToLogAndExceptionCodeAsInvalidEmailValue(string message, string exceptionMessage)
        {
            Message = message;
            GivenException = new Exception(exceptionMessage);
        }

        [When(@"I try to log error")]
        public void WhenITryToLogError()
        {
            _logger.AddLogError(Message, GivenException);
            _logger.Close();
        }

        [Then(@"The messageLog and exceptionText should appear in log file")]
        public void ThenTheMessageLogAndExceptionTextShouldAppearInLogFile()
        {
            using (FileStream fileStream = new FileStream(GetTestFilePath(), FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            using (StreamReader fileReader = new StreamReader(fileStream))
            {
                Assert.Contains(Message, fileReader.ReadLine());
                Assert.Contains(GivenException.Message, fileReader.ReadLine());
            }
        }

        string GetTestFilePath()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, testDirectoryPath, testFileName + $"-{ DateTime.Now.ToString("yyyyMMdd")}.txt");
        }

        string GetTestFilePathWithoutDate()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, testDirectoryPath, $"{testFileName}.txt");
        }
    }
}

