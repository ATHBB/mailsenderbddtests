﻿using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Data.Types;
using MailSenderWithTests.DataParser.Parser;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;
using Xunit;

namespace MailSenderWithTests.BDDTests.FileReaderTests
{
    [Binding]
    public class FileReaderTestsSteps
    {
        List<Person> personsFromFile;
        IEnumerable<Person> personsList;
        FileReader fileReader = new FileReader();
        const string testDirectoryPath = @"TestData\CSVFile";
        const string testFileName = "testData.csv";
        [Given(@"Persons in file")]
        public void GivenPersonsInFile(Table table)
        {
            personsList = table.CreateSet<Person>();
            InitalazeData(personsList);
        }
        [When(@"I read file with delimeter as (.*) and iteration = (.*)")]
        public void WhenIReadFileWithDelimeterAsAndIteration(string delimeter, int iteration)
        {
            personsFromFile = fileReader.GetDataFromFileFile(GetTestFilePath(), delimeter, iteration);
        }
        [Then(@"i should have same data")]
        public void ThenIShouldHaveSameData()
        {
            for (int i = 0; i < personsFromFile.Count; i++)
            {
                Assert.Equal(personsList.ToList()[i].Name, personsFromFile[i].Name);
                Assert.Equal(personsList.ToList()[i].SecondName, personsFromFile[i].SecondName);
                Assert.Equal(personsList.ToList()[i].Email, personsFromFile[i].Email);
            }

        }

        string GetTestFilePath()
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, testDirectoryPath, testFileName);
        }
        void InitalazeData(IEnumerable<Person> personsTable)
        {
            PrepareDirectoryForTestData();
            SaveDataToFile(FillDataForTest(personsTable));
        }
        void SaveDataToFile(StringBuilder data)
        {
            using (StreamWriter sw = new StreamWriter(GetTestFilePath(), true))
            {
                sw.Write(data.ToString());
                sw.Close();
            }
        }
        StringBuilder FillDataForTest(IEnumerable<Person> personsTable)
        {
            var data = new StringBuilder();
            foreach (Person testPerson in personsTable)
                data.AppendLine($"{testPerson.Name};{testPerson.SecondName};{testPerson.Email};");
            return data;
        }
        void PrepareDirectoryForTestData()
        {
            if (Directory.Exists(testDirectoryPath))
            {
                if (File.Exists(GetTestFilePath()))
                {
                    File.Delete(GetTestFilePath());
                }
            }
            else
                Directory.CreateDirectory(testDirectoryPath);
        }
    }
}
