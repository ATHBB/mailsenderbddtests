﻿Feature: FileReaderTests


@mytag
Scenario: ReadDataFromFileTest
	Given Persons in file
	| Name    | SecondName | Email          |
	| Tomek   | Siuciak    | tomk@gmail.com |
	| Kornel  | Krol       | Korn@gmail.com |
	| Mateusz | Dudziak    | Mat@gmail.com  |
	When I read file with delimeter as ; and iteration = 0
	Then i should have same data
