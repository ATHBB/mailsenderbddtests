﻿using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Data.Types;
using MailSenderWithTests.Sender;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using TechTalk.SpecFlow;
using Xunit;

namespace MailSenderWithTests.BDDTests.MailSenderTests
{
    [Binding]
    public class MailSenderTestsSteps
    {
        MailToSend mailToSend;
        MailSender mailSender;
        string testDirectoryPath = ConfigurationManager.AppSettings["directoryFilePathToTestMails"];
        string templateFilePath = @"TestData\MailTemplate.cshtml";
        [Given(@"I have created mail to send with (.*), (.*) and (.*)")]
        public void GivenIHaveCreatedMailToSend(string name, string secondName, string email)
        {
            mailToSend = new MailToSend(new Person { Email = email, Name = name, SecondName = secondName });
        }

        [When(@"I sent mail")]
        public void WhenISentMail()
        {
            mailSender = new MailSender(new Logger.Logger($"log-{DateTime.Now}.txt"));
            CreateTemplateDirectory(templateFilePath);
            mailSender.SendMail(mailToSend, templateFilePath);
        }
        [When(@"I unsuccessfully sent email")]
        public void WhenIUnsuccessfullySentMail()
        {
            mailToSend.AfterErrorSend();
        }

        [Then(@"Should be any file in directory")]
        public void ThenShouldBeAnyFileInDirectory()
        {
            Assert.True(Directory.GetFiles(testDirectoryPath).Any());
        }

        [Then(@"ErrorCounter value should be (.*)")]
        public void ThenErrorCounterValueShouldBe(int expectedCounterValue)
        {
            Assert.Equal(expectedCounterValue, mailToSend.ErrorCounter);
        }
        void CreateTemplateDirectory(string templateFilePath)
        {
            if (Directory.Exists(testDirectoryPath))
                Array.ForEach(Directory.GetFiles(testDirectoryPath), File.Delete);
            else
                Directory.CreateDirectory(testDirectoryPath);
        }

    }
}
