﻿Feature: MailSenderTests

@mytag
Scenario: SendMailTest
	Given I have created mail to send with Imie, Nazwisko and imie@nazwisko.com
	When I sent mail
	Then Should be any file in directory

Scenario: ErrorCounterValueTest
	Given I have created mail to send with Imie, Nazwisko and imie@nazwisko.com
	When I unsuccessfully sent email
	Then ErrorCounter value should be 1
