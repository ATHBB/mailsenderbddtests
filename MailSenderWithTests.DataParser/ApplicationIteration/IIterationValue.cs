﻿namespace MailSenderWithTests.DataParser.ApplicationIteration
{
    public interface IIterationValue
    {
        int? GetIterationValue();
        void IncrementIterationValue();
    }
}