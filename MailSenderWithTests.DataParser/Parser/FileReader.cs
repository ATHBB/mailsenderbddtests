﻿using CsvHelper;
using CsvHelper.Configuration;
using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Data.Types;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace MailSenderWithTests.DataParser.Parser
{
    public class FileReader : IFileReader
    {
        public List<Person> GetDataFromFileFile(string filePath, string delimeter, int iteration)
        {
            if (string.IsNullOrWhiteSpace(filePath)) throw new Exception(ExceptionTexts.dataFileValueIsNullOrWhitespace);
            if (delimeter == null) throw new Exception(ExceptionTexts.delimeterValueIsNull);
            if ( iteration < 0) throw new ArgumentOutOfRangeException(nameof(iteration), ExceptionTexts.invalidIterationValue);
            if (File.Exists(filePath) == false) throw new Exception(ExceptionTexts.dataFileDoesntExist);
            using (var streamReader = new StreamReader(filePath))
            {
                var csv = new CsvReader(streamReader, new CsvConfiguration
                {
                    HasHeaderRecord = false,
                    Delimiter = delimeter
                });
                return csv.GetRecords<Person>().Skip(iteration * 100).Take(100).ToList();
            }
        }
    }
}
