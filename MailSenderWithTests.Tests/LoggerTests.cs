﻿using MailSenderWithTests.Data.Application;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace MailSenderWithTests.Tests
{
    [TestClass]
    public class LoggerTests
    {
        [TestMethod]
        public void AddLogEntryEmptyMessageTest()
        {
            var logger = new Logger.Logger();
            try
            {
                logger.AddLogEntry(null);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, ExceptionTexts.emptyMessageToLog);
                return;
            }
            Assert.Fail("Exception not thrown");
        }

        [TestMethod]
        public void AddLogErrorEmptyMessageTest()
        {
            var logger = new Logger.Logger();
            try
            {
                logger.AddLogError(null, null);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, ExceptionTexts.emptyMessageToLog);
                return;
            }
            Assert.Fail("Exception not thrown");
        }

        [TestMethod]
        public void AddLogErrorNullExceptionTest()
        {
            var logger = new Logger.Logger();
            try
            {
                logger.AddLogError("aaa", null);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, ExceptionTexts.exceptionToLogIsNull);
                return;
            }
            Assert.Fail("Exception not thrown");
        }
    }
}
