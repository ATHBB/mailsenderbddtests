﻿using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Data.Types;
using MailSenderWithTests.Sender;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Configuration;
using System.IO;
using System.Linq;

namespace MailSenderWithTests.Tests
{
    [TestClass]
    public class MailSenderTests
    {
        [TestMethod]
        public void SendMailInvalidDataToSendTest()
        {
            var mailToSend = new MailToSend(new Person());
            var mailSender = new MailSender(new Logger.Logger());
            try
            {
                mailSender.SendMail(mailToSend, null);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, ExceptionTexts.dataToSendIsInvalid);
                return;
            }
            Assert.Fail("Exception not thrown");
        }

        [TestMethod]
        public void SendMailInvalidEmailValueTest()
        {
            var mailToSend = new MailToSend(new Person { Email = "email", Name = "name", SecondName = "secondName" });
            var mailSender = new MailSender(new Logger.Logger());
            try
            {
                mailSender.SendMail(mailToSend, null);
            }
            catch (Exception ex)
            {
                StringAssert.Contains(ex.Message, ExceptionTexts.invalidEmailValue);
                return;
            }
            Assert.Fail("Exception not thrown");
        }

        [TestMethod]
        public void SendMailErrorCounterValueTest()
        {
            var mailToSend = new MailToSend(new Person { Email = "email@test.com", Name = "name", SecondName = "secondName" });
            var mailSender = new MailSender(new Logger.Logger());
            for (int i = 0; i <= 5; i++)
            {
                mailToSend.AfterErrorSend();
            }
            try
            {
                mailSender.SendMail(mailToSend, null);
            }
            catch (ArgumentOutOfRangeException ex)
            {
                StringAssert.Contains(ex.Message, ExceptionTexts.invalidErrorCounterValue);
                return;
            }
            Assert.Fail("Exception not thrown");
        }

        [TestMethod]
        public void SendMail()
        {
            var testDirectoryPath = ConfigurationManager.AppSettings["directoryFilePathToTestMails"];
            var mailTemplatePath = @"TestData\MailTemplate.cshtml";
            if (string.IsNullOrWhiteSpace(testDirectoryPath)) throw new Exception("Config value for test file path is null or empty");
            if (Directory.Exists(testDirectoryPath))
                Array.ForEach(Directory.GetFiles(testDirectoryPath), File.Delete);
            else
                Directory.CreateDirectory(testDirectoryPath);
            var mailToSend = new MailToSend(new Person { Email = "email@test.com", Name = "name", SecondName = "secondName" });
            var mailSender = new MailSender(new Logger.Logger());
            mailSender.SendMail(mailToSend,mailTemplatePath);
                Assert.IsTrue(Directory.GetFiles(testDirectoryPath).Any());
        }
    }
}
