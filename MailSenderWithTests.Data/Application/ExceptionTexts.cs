﻿namespace MailSenderWithTests.Data.Application
{
    public static class ExceptionTexts
    {
        public const string dataFileValueIsNullOrWhitespace = "path for data file is null or have only white spaces";
        public const string dataFileDoesntExist = "data file doesnt exist";
        public const string delimeterValueIsNull = "value for delimeter is unset";
        public const string settingMailToSendErrorWhileWasSend = "email was send, cant set eroor counter";
        public const string invalidIterationValue = "iteration value < 0";
        public const string nullOrEmptyIterationValue = "iteration value from config is null or empty";
        public const string sendMailErrorMessage = "an error occurred while send email";
        public const string dataToSendIsInvalid = "data to send is invalid";
        public const string invalidErrorCounterValue = "error counter <0 or error counter >5";
        public const string emptyMessageToLog = "message to log is empty";
        public const string exceptionToLogIsNull = "exception to log is null";
        public const string invalidEmailValue = "email value is incorrect";
    }
}
