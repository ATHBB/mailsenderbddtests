﻿using System.Configuration;

namespace MailSenderWithTests.Data.Application
{
    public static class ApplicationSettings
    {
        public static string MailBodyTemplateName { get { return ConfigurationManager.AppSettings[nameof(MailBodyTemplateName)]; } }
        public static string DataFilePath { get { return ConfigurationManager.AppSettings[nameof(DataFilePath)]; } }
        public static string Delimeter { get { return ConfigurationManager.AppSettings[nameof(Delimeter)]; } }
        public static string ServiceName { get { return ConfigurationManager.AppSettings[nameof(ServiceName)]; } }
    }
}
