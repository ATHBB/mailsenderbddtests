﻿using MailSenderWithTests.Data.Application;
using System;
using System.Net.Mail;

namespace MailSenderWithTests.Data.Types
{
    public class MailToSend
    {
        public Person MailData { get; }

        int errorCounter;
        public int ErrorCounter { get { return errorCounter; } }
        bool wasSend;
        public bool WasSend { get { return wasSend; } }

        public MailToSend(Person mailData)
        {
            MailData = mailData;
        }

        public void AfterSuccessfullySend()
        {
            wasSend = true;
        }

        public void AfterErrorSend()
        {
            if (wasSend) throw new Exception(ExceptionTexts.settingMailToSendErrorWhileWasSend);
            errorCounter++;
        }

        public bool VerifyEmail()
        {
            try
            {
                var result = new MailAddress(MailData.Email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
    }

}
