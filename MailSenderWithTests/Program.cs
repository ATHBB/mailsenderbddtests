﻿using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Depadency;
using MailSenderWithTests.Schedule;
using MailSenderWithTests.Service;
using Quartz;
using Topshelf;
using Topshelf.Ninject;
using Topshelf.Quartz;
using Topshelf.Quartz.Ninject;

namespace MailSenderWithTests
{
    class Program
    {
        static void Main()
        {
            HostFactory.Run(c =>
            {
                c.UseNinject(new MailSenderModule());
                c.Service((Topshelf.ServiceConfigurators.ServiceConfigurator<WindowsService> s) =>
                {
                    s.ConstructUsingNinject();
                    s.WhenStarted((service, control) => service.OnStart());
                    s.WhenStopped((service, control) => service.OnStop());
                    s.UseQuartzNinject();
                    s.ScheduleQuartzJob(q =>
                        q.WithJob(() =>
                            JobBuilder.Create<SendEmailJob>().Build())
                            .AddTrigger(() =>
                                TriggerBuilder.Create()
                                    .WithSimpleSchedule(builder => builder.WithIntervalInSeconds(10).RepeatForever()).Build()));
                });
                c.RunAsLocalService();
                c.SetDisplayName(ApplicationSettings.ServiceName);
            });
        }
    }
}
