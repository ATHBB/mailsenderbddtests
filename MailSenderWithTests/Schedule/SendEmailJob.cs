﻿using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Data.Types;
using MailSenderWithTests.DataParser.ApplicationIteration;
using MailSenderWithTests.DataParser.Parser;
using MailSenderWithTests.Logger;
using MailSenderWithTests.Sender;
using Quartz;
using System;
using System.Linq;

namespace MailSenderWithTests.Schedule
{
    public class SendEmailJob : IJob
    {
        readonly IMailSender _mailSender;
        readonly IFileReader _fileReader;
        readonly ILogger _logger;
        readonly IIterationValue _iteration;

        public SendEmailJob(IMailSender mailSender, IFileReader fileReader, ILogger logger, IIterationValue iteration)
        {
            _mailSender = mailSender;
            _fileReader = fileReader;
            _logger = logger;
            _iteration = iteration;
        }

        public void Execute(IJobExecutionContext context)
        {
            try
            {
                var iteration = _iteration.GetIterationValue();
                if(iteration.HasValue)
                {
                    var parsedData = _fileReader.GetDataFromFileFile(ApplicationSettings.DataFilePath, ApplicationSettings.Delimeter, iteration.Value);
                    _iteration.IncrementIterationValue();
                    parsedData.Select(x => new MailToSend(x)).ToList().ForEach(x =>
                    {
                        _mailSender.SendMail(x, ApplicationSettings.MailBodyTemplateName);
                    });
                }
            }
            catch (Exception ex)
            {
                _logger.AddLogError("An exception occured while getting data from file", ex);
            }
        }
    }
}

