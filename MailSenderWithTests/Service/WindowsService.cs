﻿using MailSenderWithTests.Data.Application;
using MailSenderWithTests.Logger;
using System.Configuration;
namespace MailSenderWithTests.Service
{
    public class WindowsService
    {
        ILogger _logger;

        public WindowsService(ILogger logger)
        {
            _logger = logger;
        }

        public bool OnStart()
        {
            
            return true;
        }

        public bool OnStop()
        {
            return true;
        }
    }
}
